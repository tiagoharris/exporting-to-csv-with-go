//  This Go script exports a MySQL table to a CSV file.
//
//  author: Tiago Melo (tiagoharris@gmail.com)

package main

import (
	"database/sql"
	"encoding/csv"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"os"
	"strconv"
	"time"
)

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func main() {
	// these are the variables that will hold the data for each row in the table
	var (
		id           int
		name         string
		email        string
		phone_number string
		birth_date   time.Time
	)

	// sql.Open does not return a connection. It just returns a handle to the database.
	// passing 'parseTime=true' means that any DATE field on the table will be automatically
	// mapped to 'time.Time'.
	db, err := sql.Open("mysql", "root:@/spring_batch_example?parseTime=true")

	// A defer statement pushes a function call onto a list.
	// The list of saved calls is executed after the surrounding function returns.
	// Defer is commonly used to simplify functions that perform various clean-up actions.
	defer db.Close()
	
	checkError("Error getting a handle to the database", err)

	// Now it's time to validate the Data Source Name (DSN) to check if the connection
	// can be correctly established.
	err = db.Ping()

	checkError("Error establishing a connection to the database", err)

	rows, err := db.Query("SELECT * FROM user")
	
	defer rows.Close()

	checkError("Error creating the query", err)

	file, err := os.Create("result.csv")

	defer file.Close()
	
	checkError("Error creating the file", err)

	writer := csv.NewWriter(file)
	defer writer.Flush()

	// this is the slice that will be appended with rows from the table
	s := make([][]string, 0)

	// now let's loop through the table lines and append them to the slice declared above
	for rows.Next() {
		// read the row on the table; it has five fields, and here we are
		// assigning them to the variables declared above
		err := rows.Scan(&id, &name, &email, &phone_number, &birth_date)

		checkError("Error reading rows from the table", err)

		// appending the row data to the slice
		s = append(s, []string{strconv.Itoa(id), name, email, phone_number, birth_date.String()})
	}

	err = rows.Err()

	checkError("Error reading rows from the table", err)

	// now we loop through the slice and write the lines to CSV file
	for _, value := range s {
		err := writer.Write(value)

		checkError("Error writing line to the file", err)
	}
}
